#!/bin/bash
set -e

VIM_REPO=${1}

# Prerequisits:
# * Powerline fonts, and a 256 color terminal.
#   Note: This may be problematic for some Windows terminal emulators.

if [ -d ~/.vim ]
then
    echo "Found an existing ~/.vim folder. Moving it to ~/.vim.bak"
    mv --backup=t ~/.vim ~/.vim.bak
fi

if [ -d ~/.vim_runtime ]
then
    echo "Found an existing ~/.vim_runtime folder. Moving it to ~/.vim_runtime.bak"
    mv --backup=t ~/.vim_runtime ~/.vim_runtime.bak
fi

if [ -f ~/.vimrc ]
then
    echo "Found an existing ~/.vimrc file. Moving it to ~/.vimrc.bak"
    mv --backup=t ~/.vimrc ~/.vimrc.bak
fi

if [[ "$VIM_REPO" == "" ]]
then
    mv $(dirname $0) ~/.vim
else
    git clone $VIM_REPO ~/.vim
fi

ln -fs ~/.vim/vimrc ~/.vimrc
ln -fs ~/.vim/vim_runtime ~/.vim_runtime
ln -fs ~/.vim/vim_runtime/sources_non_forked/colorschemes/colors ~/.vim/colors

if [ -d ~/.vim_runtime/sources_non_forked/command-t/ruby/command-t ]
then
    echo "Setting up Command-t vim plugin."
    sudo apt-get -y install ruby ruby-dev build-essential
    pushd ~/.vim_runtime/sources_non_forked/command-t/ruby/command-t
    ruby extconf.rb
    make
    popd
fi

if [ -d ~/.vim_runtime/sources_non_forked/YouCompleteMe ]
then
    echo "Setting up YouCompleteMe vim plugin."
    sudo apt-get -y install cmake build-essential clang clang-format
    pushd ~/.vim_runtime/sources_non_forked/YouCompleteMe
    ./install.py --clang-completer
    popd
fi
