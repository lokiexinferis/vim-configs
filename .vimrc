execute pathogen#infect()
filetype plugin indent on

" You can use other keymappings like <C-l> instead of <CR> if you want to
" use these mappings as default search and somtimes want to move cursor with
" EasyMotion.
function! s:incsearch_config(...) abort
  return incsearch#util#deepextend(deepcopy({
  \   'modules': [incsearch#config#easymotion#module({'overwin': 1})],
  \   'keymap': {
  \     "\<CR>": '<Over>(easymotion)'
  \   },
  \   'is_expr': 0
  \ }), get(a:, 1, {}))
endfunction

function! s:config_easyfuzzymotion(...) abort
  return extend(copy({
  \   'converters': [incsearch#config#fuzzy#converter()],
  \   'modules': [incsearch#config#easymotion#module()],
  \   'keymap': {"\<CR>": '<Over>(easymotion)'},
  \   'is_expr': 0
  \ }), get(a:, 1, {}))
endfunction

" These are the exact string matching search commands.
noremap <silent><expr> z/  incsearch#go(<SID>incsearch_config())
noremap <silent><expr> z?  incsearch#go(<SID>incsearch_config({'command': '?'}))
noremap <silent><expr> zg/ incsearch#go(<SID>incsearch_config({'is_stay': 1}))

" These are the fuzzy string matching search commands.
noremap <silent><expr> /  incsearch#go(<SID>config_easyfuzzymotion())
noremap <silent><expr> ?  incsearch#go(<SID>config_easyfuzzymotion({'command': '?'}))
noremap <silent><expr> g/ incsearch#go(<SID>config_easyfuzzymotion({'is_stay': 1}))

" The maximum line length to warn about, in characters.
let linelengthlimit = 80
" The first character that is considered over the length. Should always be
" linelengthlimit + 1
let upper_warning_length = linelengthlimit + 1
" The first character to start an early warning for, if enabled.
let lower_warning_length = linelengthlimit - 3

" The filetypes to add line length warnings to.
let filemask = "*.c,*.cc,*.h,*.py,*.sh,*.zsh,*.ino"

" The warnings/highlights to enable
let warning_column = 0
let early_length_warning = 0
let long_line_warning = 1
let mark_extra_whitespace = 1
" How big to make the tabs when displayed
let tabsize = 4

" show existing tab with tabsize spaces width
execute "set tabstop=".tabsize
" when indenting with '>', use tabsize spaces width
execute "set shiftwidth=".tabsize

execute "set softtabstop=".tabsize
" On pressing tab, insert tabstop spaces
set expandtab
set smartindent
set autoindent

" Enable syntax highlighting
syntax on
set background=dark
colorscheme solarized
hi Normal guibg=NONE ctermbg=NONE

if mark_extra_whitespace
    highlight ExtraWhitespace ctermbg=red guibg=red
    match ExtraWhitespace /\s\+$/
    autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
    autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
    autocmd InsertLeave * match ExtraWhitespace /\s\+$/
    autocmd BufWinLeave * call clearmatches()
endif

if warning_column
    " Add a marker at the first over-length character.
    execute 'set colorcolumn=' . upper_warning_length
    highlight ColorColumn ctermbg=0 guibg=lightgrey
endif

" Make the vertical split bar much nicer
autocmd ColorScheme * highlight VertSplit cterm=NONE ctermfg=Green ctermbg=NONE
set encoding=utf8
set fillchars+=vert:│

if early_length_warning
    " Highlight text that is close to the character line limit.
    execute "autocmd BufWinEnter " .
\           filemask .
\           " let w:m1=matchadd('Search', '\\%<" .
\           upper_warning_length .
\           "v.\\%>" .
\           lower_warning_length .
\           "v', -1)"
endif

if long_line_warning
    " Highlight text that is over 80 characters long using the ErrorMsg
    "highlight rule.
    execute "autocmd BufWinEnter " .
\           filemask .
\           " let w:m2=matchadd('ErrorMsg', '\\%>" .
\           linelengthlimit .
\           "v.\\+', -1)"
endif

" Automatically reload the .vimrc file if it changes.
augroup myvimrchooks
    autocmd!
    autocmd bufwritepost .vimrc source ~/.vimrc
augroup END

" Search for selected text, forwards or backwards.
vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>

set hlsearch
" Press Space to turn off highlighting and clear any message already
" displayed.
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

set wildignore=*.o,*/obj/*,obj/*,*/obj,*.pyc,bazel*/*,coverage/*

" Prevent vim from automatically wrapping long lines. This tends to break things
" like python scripts.
set textwidth=0
set wrapmargin=0

" Leave 2 spaces on the status line, for powerline stuff
set laststatus=2

" Enable 256 color mode
set t_Co=256

" Enable mouse mode, for full mouse control.
set mouse=a
" Let the mouse work correctly past te 220th column.
set ttymouse=sgr

" Set the default open locations of new splits to below and to the right.
set splitbelow
set splitright
"

" Disables equal splitting when performing splits. This makes it so that a
" split will only split the current window and not resize the others. Also
" when closing a split (such as a preview window) the other splits will be
" unaffected by it.
set noequalalways

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" Set some airline settings
"let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='powerlineish'
let g:airline_powerline_fonts=1

" Allowing YCM files to be autoloaded without asking.
let g:ycm_confirm_extra_conf = 0
