"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Important:
"       This requries that you install https://github.com/amix/vimrc !
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" pathogen
" {
    if filereadable(expand("~/.vim_runtime/autoload/pathogen.vim"))
        let s:vim_runtime = expand('<sfile>:p:h')."/.."
        call pathogen#infect(s:vim_runtime.'/sources_forked/{}')
        call pathogen#infect(s:vim_runtime.'/sources_non_forked/{}')
        call pathogen#infect(s:vim_runtime.'/my_plugins/{}')
        call pathogen#helptags()
    endif
" }

" bufExplorer plugin
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/bufexplorer/"))
        let g:bufExplorerDefaultHelp=0
        let g:bufExplorerShowRelativePath=1
        let g:bufExplorerFindActive=1
        let g:bufExplorerSortBy='name'
        map <leader>o :BufExplorer<cr>
    endif
" }

" MRU
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/mru.vim/"))
        let MRU_Max_Entries = 400
        map <leader>f :MRU<CR>
    endif
" }

" YankStack
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/vim-yankstack/"))
        let g:yankstack_yank_keys = ['y', 'd']
        nmap <c-p> <Plug>yankstack_substitute_older_paste
        nmap <c-n> <Plug>yankstack_substitute_newer_paste
    endif
" }

" Ctags
" {
    set tags=./tags;/,~/.vimtags

    " Make tags placed in .git/tags file available in all levels of a repository
    let gitroot = substitute(system('git rev-parse --show-toplevel'), '[\n\r]', '', 'g')
    if gitroot != ''
        let &tags = &tags . ',' . gitroot . '/.git/tags'
    endif
" }

" CTRL-P
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/ctrlp.vim/"))
        let g:ctrlp_working_path_mode = 0

        let g:ctrlp_map = '<c-f>'
        map <leader>j :CtrlP<cr>
        map <c-b> :CtrlPBuffer<cr>

        let g:ctrlp_max_height = 20
        let g:ctrlp_custom_ignore = 'node_modules\|^\.DS_Store\|^\.git\|^\.coffee'
    endif
" }

" snipMate
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/vim-snipmate/"))
        ino <c-j> <c-r>=snipMate#TriggerSnippet()<cr>
        snor <c-j> <esc>i<right><c-r>=snipMate#TriggerSnippet()<cr>
    endif
" }

" Vim grep
" {
    let Grep_Skip_Dirs = 'RCS CVS SCCS .svn generated .git'
    set grepprg=/bin/grep\ -nH
" }

" Nerd Tree
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/nerdtree/"))
        let NERDTreeShowBookmarks=1
        let NERDTreeIgnore=['\.py[cd]$', '__pycache__', '\~$', '\.swo$', '\.swp$', '^\.git$', '^\.hg$', '^\.svn$', '\.bzr$']
        let NERDTreeChDirMode=0
        let NERDTreeQuitOnOpen=1
        let NERDTreeMouseMode=2
        let NERDTreeShowHidden=1
        let NERDTreeKeepTreeInNewTab=1
        map <leader>nn :NERDTreeToggle<cr>
        map <leader>nb :NERDTreeFromBookmark<Space>
        map <leader>nf :NERDTreeFind<cr>
    endif
" }

" vim-multiple-cursors
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/vim-multiple-cursors/"))
        let g:multi_cursor_next_key="\<C-s>"
    endif
" }

" surround.vim
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/vim-surround/"))
        vmap Si S(i_<esc>f)
        au FileType mako vmap Si S"i${ _(<esc>2f"a) }<esc>
    endif
" }


" airline
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/vim-airline/"))
        let g:airline_theme='powerlineish'
        let g:airline_powerline_fonts=1
        let g:airline#extensions#tabline#enabled = 1
        let g:airline#extensions#ale#enabled = 1
    endif
" }

" Vim-go
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/vim-go/"))
        let g:go_fmt_command = "goimports"
    endif
" }

" Ale
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/ale/"))
        let g:ale_linters = {
        \   'c': ['clang-format'],
        \   'cpp': ['clang-format'],
        \   'javascript': ['jshint'],
        \   'python': ['flake8'],
        \   'go': ['go', 'golint', 'errcheck']
        \}

        nmap <silent> <leader>a <Plug>(ale_next_wrap)

        " Disabling highlighting
        let g:ale_set_highlights = 0

        " Only run linting when saving the file
        let g:ale_lint_on_text_changed = 'never'
        let g:ale_lint_on_enter = 0
    endif
" }

" Git gutter
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/vim-gitgutter/"))
        let g:gitgutter_enabled=1

        " Let vim update the gutter every 100ms. The default is 4 seconds.
        set updatetime=100
        nnoremap <silent> <leader>d :GitGutterToggle<cr>

        if exists('&signcolumn')  " Vim 7.4.2201
            set signcolumn=yes
        else
            let g:gitgutter_sign_column_always = 1
        endif
    endif
" }

" Fugitive
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/vim-fugitive/"))
        nnoremap <silent> <leader>gs :Gstatus<CR>
        nnoremap <silent> <leader>gd :Gdiff<CR>
        nnoremap <silent> <leader>gc :Gcommit<CR>
        nnoremap <silent> <leader>gb :Gblame<CR>
        nnoremap <silent> <leader>gl :Glog<CR>
        nnoremap <silent> <leader>gp :Git push<CR>
        nnoremap <silent> <leader>gr :Gread<CR>
        nnoremap <silent> <leader>gw :Gwrite<CR>
        nnoremap <silent> <leader>ge :Gedit<CR>
        " Mnemonic _i_nteractive
        nnoremap <silent> <leader>gi :Git add -p %<CR>
        nnoremap <silent> <leader>gg :SignifyToggle<CR>
    endif
" }


" Easymotion
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/vim-easymotion/"))
        " You can use other keymappings like <C-l> instead of <CR> if you want to
        " use these mappings as default search and somtimes want to move cursor with
        " EasyMotion.
        function! s:incsearch_config(...) abort
            return incsearch#util#deepextend(
            \   deepcopy({
            \       'modules': [incsearch#config#easymotion#module({'overwin': 1})],
            \       'keymap': {
            \           "\<CR>": '<Over>(easymotion)'
            \       },
            \       'is_expr': 0}),
            \   get(a:, 1, {}))
        endfunction

        noremap <silent><expr> /  incsearch#go(<SID>incsearch_config())
        noremap <silent><expr> ?  incsearch#go(<SID>incsearch_config({'command': '?'}))
        noremap <silent><expr> g/ incsearch#go(<SID>incsearch_config({'is_stay': 1}))
    endif

" }

" indent_guides
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/vim-indent-guides/"))
        let g:indent_guides_start_level = 2
        let g:indent_guides_guide_size = 1
        let g:indent_guides_enable_on_vim_startup = 0
    endif
" }

" YouCompleteMe
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/YouCompleteMe/"))
        let g:acp_enableAtStartup = 0

        " enable completion from tags
        let g:ycm_collect_identifiers_from_tags_files = 1

        " Enable omni completion.
        autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
        autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
        autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
        autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
        autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
        autocmd FileType ruby setlocal omnifunc=rubycomplete#Complete
        autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc

        " Haskell post write lint and check with ghcmod
        " $ `cabal install ghcmod` if missing and ensure
        " ~/.cabal/bin is in your $PATH.
        if !executable("ghcmod")
            autocmd BufWritePost *.hs GhcModCheckAndLintAsync
        endif

        " For snippet_complete marker.
        if has('conceal')
            set conceallevel=2 concealcursor=i
        endif

        " Disable the neosnippet preview candidate window
        " When enabled, there can be too much visual noise
        " especially when splits are used.
        set completeopt-=preview
    endif
" }

" localvimrc
" {
    if isdirectory(expand("~/.vim_runtime/sources_non_forked/vim-localvimrc/"))
        let g:localvimrc_whitelist=['/sx/', '/home/\(ikelner\|ilya\)/.vim/vim_runtime/repo_specific_files/']
        let g:localvimrc_sandbox=0
    endif
" }
