This repo contains my current vim configuration, to make it easy to redeploy on
other machines.

This repo should be instantiated as the .vim/ directory in the home folder (~)
of a machine. The .vimrc should be placed a directory above, or symlinked there.
Everything else should be self sufficient.
