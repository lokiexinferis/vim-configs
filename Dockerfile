from ubuntu:16.04

ENV HOME /root
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y apt-utils software-properties-common sudo && add-apt-repository ppa:jonathonf/vim

RUN apt-get update && apt-get install -y vim-nox git python python-pip

ADD . /tmp/vim

RUN /tmp/vim/setup.sh
